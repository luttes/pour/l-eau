

- https://oclibertaire.lautre.net/spip.php?article3679

La manifestation contre le pillage de l'eau des Alpes par la société
STmicro à Crolle (isère) est le versant du combat pour la sauvegarde
de l'eau contre les intérêts industriels, comme Sainte-Soline est celui
contre son appropriation par l'agro-industrie productiviste.

Deux axes d'une même lutte pour la sauvegarde des biens communs de l'humanité.

Mais contrer l'expansion de STmicro comprend aussi une dimension antimilitariste que viennent ici rappeler nos camarades du CRAMM (collectif régional anti-armement et militarisme). ---- PUCES, TOUJOURS PUCES POUR LES MILITAIRES, TOUJOURS MOINS POUR L'EAU ET LES POPULATIONS LOCALES ---- Puce-toi de là que j'm'y mette! ---- Le projet de deuxième usine STMicro sur le site de Crolles est du jamais vu. Ce projet de 5,3 milliards d'euros sera financé à 43 % (soit 2,5 M) sur fonds publics: du jamais vu car hors de toutes les règles européennes sur les financements publics au secteur privé. Il révèle tout le mépris que les industriels, capitalistes, pouvoirs publics, État et Commission Européenne ont pour les habitants, la nature et l'eau nécessaire à la vie.

Les populations sont priées d'applaudir car rendre l'Europe autonome (Chip Act européen doté de 45 milliards) et produire en Europe 20 % des puces mondiales, est à ce prix. Cet accaparement de la ressource en eau est justifié une fois de plus au nom du progrès, de l'indépendance nationale et la compétitivité.

Les locaux et les autres, doivent aussi comprendre que toujours plus de smartphones, d'objets connectés, de voitures autonomes, de satellites, de villes intelligentes c'est pour leur bien car le progrès est dans la connexion permanente... Tout en les laissant dans l'ignorance de la dimension militaire et sécuritaire du projet.

Derrière les usines de puces la logique mortifère d'intérêts militaro-industriels bien cachés

L'industrie électronique et informatique grenobloise a un long passé de liens organiques avec le complexe universitaro-militaro-industriel local et national dans la conception et la vente de matériels militaires en France comme à l'étranger.

SOITEC a été créée pour «valoriser» les technologies «Sicilium Sur Isolant» développées par le CEA-Direction des Applications Militaires et le CEA/LETI afin de «répondre à des besoins très spécifiques des programmes de la dissuasion[nucléaire]».

En 2018, SOITEC a racheté avec MBDA (pour une poignée de puces), l'entreprise Dolphin Integration (Meylan) qui conçoit des circuits intégrés pour l'industrie de l'armement. Le fabricant de missiles MBDA étant depuis 2004 le premier client de Dolphin, une nouvelle ère de développement et de profitabilité se profile pour ces 3 boîtes.

STMicroelectonics (entreprise italo-française de droit suisse) exploite des technologies de SOITEC pour le civil comme le militaire. En 2020 la filiale grenobloise de STMicroelectronics prend la tête du consortium d'industriels européens EXCEED, financé par Agence Européenne de Défense (AED) pour développer les bases d'une filière européenne de «systèmes sur puce» destinés à des applications militaires.

L'objectif: supprimer la dépendance aux puces américaines et contourner l'interdiction de vente de matériels militaires ou stratégiques contenant des composants américains, à des ennemis des USA (réglementation ITAR, International Traffic in Arms Regulations).

Le but final: pouvoir vendre librement n'importe quel matériel militaire, à n'importe qui, mais à un bon prix.

STMicro, Lynred et SOITEC: des marchands de mort déguisés en bisounours de la vie connectée

STMicro: ses publicités prétendent «augmenter la vie». En réalité, elles la raccourcissent brutalement pour beaucoup d'humains en Ukraine et ailleurs.

Ses microcontrôleurs STM32 ont été retrouvés dans une série de drones russes engagés en Ukraine: Orlan-10, E95M, Eleron-3SV et Koub-BlA. Des puces STMicro ont aussi été retrouvées dans les systèmes de guidage des missiles high tech Kh-101, utilisés contre les civils et les infrastructures stratégiques. SOFRADIR/ Lynred (Veurey-Voroise): Le CEA/LETI est à l'origine de l'entreprise créée pour «valoriser» ses découvertes dans l'infrarouge: caméras thermiques, systèmes de guidages... C'est maintenant une filiale de Thalès et Safran.

Ses caméras ont été retrouvées (avec des caméras Catherine FC de Thalès) sur des chars russes capturés. Comme le révèle un article du Progrès du 24 mars, ses détecteurs PICO 640-046commercialisés en 2015 (un an après l'embargo sur le matériel militaire français vers la Russie) et fabriquées en 2022, équipent les systèmes de détection des drones Orlan lancés sur l'Ukraine. SOITEC qui vend ses technologies à toute l'industrie militaire, continue paisiblement à fournir la force de frappe nucléaire.

Bien évidemment ces informations ne sont que la partie émergée de l'iceberg et la réalité de l'implication réelle de ces entreprises et bien d'autres dans les guerres et répressions contemporaines est immense et cachée (secret des affaires, confidentiel défense, ventes clandestines...).

Un État et des entreprises, hypocrites et menteurs

Beaucoup de ces puces sont des «biens à double usage», à la fois civils et militaires. Ce flou organisé permet à la Métropole de Grenoble, aux entreprises concernées et aux syndicats de prétendre qu'ils ne s'agit que de matériel civils que l'on peut continuer à vendre sans problèmes. C'est d'ailleurs la pitoyable défense de Lynred en réponse à l'article du Progrès déjà cité.

L'embargo de l'Union européenne visant la Russie, édicté en 2014 interdit l'exportation de ce type de biens «s'ils sont destinés entièrement, ou en partie, à un usage militaire, ou à un utilisateur final militaire». Comment se fait-il donc que toutes ces puces et détecteurs se retrouvent actuellement en Ukraine?

Plus globalement de nombreuses lois, traités et conventions internationales sont censées réglementer, contrôler, «moraliser» le commerce des armes. Officiellement le TCA (Traité sur le Commerce des Armes) signé par la France, interdit d'exporter des armes «vers des pays ou des zones de conflits où les droits humains sont bafoués».

En France c'est l'État et lui seul, qui choisit ce qu'il vend et à qui, en dehors de tout contrôle parlementaire ou autre. Il est donc le principal responsable des conséquences de ces ventes. Les populations du Yémen, du Togo, de Palestine, d'Égypte, d'Indonésie, du Chili, du Sénégal, de Bahreïn, du Kurdistan le savent bien elles, qui sont victimes de guerres ou de répressions menées avec du matériel français.

Un marché protégé qui rapporte gros, dopé par les budgets militaires...

C'est l'État qui finance la recherche/développement militaire ou à double usage, achète pour son armée et qui, accompagné par les représentants corrompus des fabricants, sert de VRP à l'exportation. Au passage, il récupère 2% sur toutes ces ventes. Et comme celles-ci sont vitales à son industrie militaire pour lui permettre de continuer à exister, il faut toujours exporter plus.

Selon le communiqué du SIPRI de mars 2023, les exportations d'armes françaises ont bondi de 44 % en 2018-2022 par rapport à la période précédente 2013-2017, avec pour premières destinations, l'Asie, l'Océanie et le Moyen-Orient. L'augmentation considérable des budgets militaires en France - 413 milliards pour 2024-2030 - soit +33 % d'augmentation - créera toujours plus de besoins en matériels toujours plus performants. Cela encourage la spirale infernale de la course mondiale aux armements. Jusqu'à où?

Les guerres se fabriquent près de chez nous et avec notre argent

Ce sont nos impôts qui permettent aux villes, départements, Régions et État de financer de nombreuses industries militaires. En Région AuRA les PME du cluster EDEN (Verney-Carron, Métravib, ARES...), Nexter (canon Caesar), Safran, Arquus (blindés), et autres Dassault (Rafale) en profitent largement. La Région finance aussi massivement (14 millions) le Plan Nano 2022 (7,5 millions l'an passé) porté par le CEA Grenoble où l'on retrouve toutes les entreprises grenobloises déjà citées.

Commerce des armes, militarisme et guerres: crimes écologiques et humains de masse

Le bilan des ravages écologiques provoqués par les conflits passés et présents est terrible: zones rouges de la guerre de 14-18, millions de tonnes de munitions (explosives et chimiques) immergées en mer du Nord), pollution des munitions à uranium appauvri en Bosnie et Irak, milliers de km2 pollués par les mines (Irak, Ukraine, Cambodge), agent Orange et opérations de modification du climat durant la guerre du Vietnam, essais nucléaires français (Algérie et Polynésie).

Les armées se verdissent... Comme au camp de Chambaran (près de Roybon) ou entre deux séances de tirs et de manoeuvres, des stagiaires réhabilitent les zones humides. Pendant ce temps, les canonniers du camps de Canjuers foutent le feu à la garrigue en pleine canicule de 2022.

Les armées sont écologiquement responsables... Comme les armées US qui ont obtenu que leur empreinte carbone soit exclue des calculs du GIEC, alors qu'elles sont les plus grandes consommatrices institutionnelles de pétrole brut au monde.

Depuis la fin de la 2ème boucherie mondiale (60 à 75 millions de morts), des dizaines de millions d'humains, majoritairement des civils, sont morts dans des conflits «localisés», avec leur cortège de populations, humiliées, terrorisées, réprimées, violées... entraînant des crises humanitaires majeures: migrations forcées, famines, épidémies...

Industries de mort et de répression:
refusons d'être complices!

Aucun syndicat, aucun parti de «gouvernement», aucune collectivité locale ne dénonce les industries de mort ici ou ailleurs: les profits, l'attractivité économique, la «préservation» et le «développement» de l'emploi (200 000 en France), le mythe de l'indépendance nationale, l'aliénation technicienne, la fascination morbide pour la puissance des armes écrasent toute discussion, critique ou contestation de même que la guerre en Ukraine

L'État avec sa réforme des retraites nous annonce maintenant qu'il va falloir travailler plus longtemps. Pour la défense de la patrie ou pour celle des industriels?

Où va-t-il trouver l'argent pour que le budget des armées atteigne les 3% du PIB? Cela se fera aux dépens des retraites, de l'éducation, de la santé, du social... En attendant d'envoyer la jeunesse embrigadée par le Service National Universel se faire massacrer pour défendre l'état et le capitalisme?

Nous appelons à dénoncer et à combattre activement cette industrie de destruction (eaux , milieux, humains), l'État, les politiques et les universitaires qui la nourrissent. Ces salopards sont complices et responsables de massacres, de guerres interminables au services d'États, de dictatures, de multinationales et d'impérialismes variés.

Rejoignez la Coordination Régionale Anti Armements et Militarisme!

PUCES MORTELLES À GRENOBLE: ÉRADIQUONS LA PESTE MILITARISTE!

https://oclibertaire.lautre.net/spip.php?article3679
_________________________________________________
